package tdd.xuchun.test.model;

public class Host {
	private int id;
	private String name = "";
	private String addr = "";
	private int port;
	private String username;
	private String password;
	private String remark;
	private int eznmonport;
	private String ostype="";
	private  String keyfilepath;

	public String getKeyfilepath() {
		return keyfilepath;
	}

	public void setKeyfilepath(String keyfilepath) {
		this.keyfilepath = keyfilepath;
	}

	public String getOstype() {
		return ostype;
	}

	public void setOstype(String ostype) {
		this.ostype = ostype;
	}

	public int getEznmonport() {
		return eznmonport;
	}

	public void setEznmonport(int eznmonport) {
		this.eznmonport = eznmonport;
	}

	public String getAddr() {
		return addr.replaceAll("\\s*", "");
	}

	public String getName() {
		return name.replaceAll("\\s*", "");
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public void setAddr(String addr) {
		this.addr = addr;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUsername() {
		return username.replaceAll("\\s*", "");
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password.replaceAll("\\s*", "");
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Host [id=" + id + ", name=" + name + ",addr=" + addr + ", port=" + port + ", username=" + username
				+ ", password=" + password + ",eznmonport=" + eznmonport + " ,remark=" + remark + " ,keyfilepath=" + keyfilepath + ",ostype=" + ostype
				+ "]";
	}

}
